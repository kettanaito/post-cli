const R = require('ramda')
const argv = require('yargs').argv
const post = require('./src')
const defaultReporter = require('./src/reporters/default')
const htmlReporter = require('./src/reporters/htmlReporter')

const url = argv._[0]

const func = R.composeP(
  R.cond([[R.always(argv.html), htmlReporter], [R.T, defaultReporter]]),
  post(url),
)

if (process.stdin.isTTY) {
  func(null)
} else {
  process.stdin.on('data', func)
}
