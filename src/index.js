const R = require('ramda')
const assert = require('assert')
const chalk = require('chalk')
const urlUtils = require('url')
const formatHeaders = require('./utils/formatHeaders')

const post = R.curry(
  (url, payload) =>
    new Promise((resolve, reject) => {
      const parsedUrl = urlUtils.parse(url)

      if (!parsedUrl.host) {
        return console.error(
          chalk.red(
            `Failed to perform a request. Expected a valid URL, but got: "${url}".`,
          ),
        )
      }

      const httpClient = require(parsedUrl.protocol.includes('https')
        ? 'https'
        : 'http')

      const reqOptions = {
        ...parsedUrl,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'User-Agent': 'post/1.0 (+https://apiary.io)',
        },
      }

      const request = httpClient.request(reqOptions, (response) => {
        const { statusCode, rawHeaders } = response

        response.setEncoding('utf8')
        response.on('data', (body) => {
          resolve({
            request,
            url: parsedUrl,
            statusCode,
            headers: rawHeaders,
            body,
          })
        })
        response.on('end', () => {
          resolve({
            request,
            url: parsedUrl,
            statusCode,
            headers: rawHeaders,
          })
        })
      })

      request.on('error', () => {
        console.error('There has been ar error with the request.', error)
        reject()
      })

      request.end(payload)
    }),
)

module.exports = post
