const R = require('ramda')
const chalk = require('chalk')
const formatHeaders = require('../utils/formatHeaders')
const formatStatusCode = require('../utils/formatStatusCode')

const insertEmptyLine = () => process.stdout.write('\n')

const logGroup = R.curry((groupName, data) => {
  process.stdout.write(chalk.gray(`${groupName}:\n`))
  return data
})

module.exports = R.evolve({
  statusCode: R.compose(
    insertEmptyLine,
    console.log,
    formatStatusCode,
    logGroup('Status code'),
  ),
  headers: R.compose(
    insertEmptyLine,
    R.forEach(console.log),
    formatHeaders,
    logGroup('Response headers'),
  ),
  body: R.compose(
    insertEmptyLine,
    console.log,
    logGroup('Response body'),
  ),
})
