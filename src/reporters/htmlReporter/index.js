const fs = require('fs')
const chalk = require('chalk')
const http = require('http')
const open = require('open')
const React = require('react')
const ReactDOMServer = require('react-dom/server')
const ServerStyleSheet = require('styled-components').ServerStyleSheet
const Reporter = require('./Reporter')

const styles = fs.readFileSync(`${__dirname}/styles.css`)
const globalStyles = styles.toString()

const PORT = 9876
const REPORTER_URL = `http://localhost:${PORT}/`

const htmlReporter = (data) => {
  const server = http.createServer((request, response) => {
    const sheet = new ServerStyleSheet()
    const html = ReactDOMServer.renderToString(
      sheet.collectStyles(React.createElement(Reporter, data)),
    )
    const styleTags = sheet.getStyleTags()

    response.end(`
      <html>
        <head>
          <title>Report</title>
          <style>${globalStyles}</style>
          ${styleTags}
        </head>
        <body>
          <div id="root">${html}</div>
        </body>
      </html>
    `)
  })

  server.listen(PORT, (error) => {
    if (error) {
      return console.error('Failed to create an HTML reporter server.', error)
    }

    console.log(' ')
    console.log(`Report is available at ${chalk.cyan(REPORTER_URL)}`)
    console.log(chalk.gray('Openning report in a browser...'))
    console.log(' ')
    open(REPORTER_URL)
  })
}

module.exports = htmlReporter
