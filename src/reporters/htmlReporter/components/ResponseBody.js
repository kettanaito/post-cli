const React = require('react')
const styled = require('styled-components').default

const PreviewPanel = styled.pre`
  padding: 1rem;
  border: 1px solid #eaecf2;
  border-radius: 8px;
  overflow: auto;
  max-height: 700px;
`

const ResponseBody = ({ body }) => React.createElement(PreviewPanel, null, body)

module.exports = ResponseBody
