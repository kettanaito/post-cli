const R = require('ramda')
const React = require('react')
const styled = require('styled-components').default

const indexedMap = R.addIndex(R.map)

const StyledHeadersList = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`

const StyledHeaderItem = styled.li`
  padding: 1rem 0;
  display: flex;

  &:not(:last-child) {
    border-bottom: 1px solid #eaecf2;
  }
`

const HeaderName = styled.span`
  display: inline-block;
  flex-shrink: 0;
  color: #545865;
  width: 50%;
`

const HeaderValuesList = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`

const HeaderValuesItem = styled.li`
  &:not(:last-child):after {
    content: '';
    display: block;
    margin: 1rem 0;
    height: 1px;
    background-color: #eaecf2;
  }
`

const HeaderItem = ({ name, values }) =>
  React.createElement(
    StyledHeaderItem,
    null,
    React.createElement(HeaderName, null, name),
    React.createElement(
      HeaderValuesList,
      null,
      values.map((value, key) =>
        React.createElement(HeaderValuesItem, { key }, value),
      ),
    ),
  )

const HeadersList = ({ headers }) => {
  return React.createElement(
    StyledHeadersList,
    null,
    R.compose(
      indexedMap(([name, values], key) =>
        React.createElement(HeaderItem, {
          key,
          name,
          values,
        }),
      ),
      R.toPairs,
      R.reduce((acc, [headerName, headerValue]) => {
        const prevValues = acc[headerName] || []
        const nextValues = prevValues.concat(headerValue)
        return R.assoc(headerName, nextValues, acc)
      }, {}),
      R.splitEvery(2),
    )(headers),
  )
}

module.exports = HeadersList
