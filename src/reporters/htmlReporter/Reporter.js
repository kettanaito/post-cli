const R = require('ramda')
const React = require('react')
const PropTypes = require('prop-types')
const styled = require('styled-components').default
const getStatusCodeColor = require('../../utils/getStatusCodeColor')
const HeadersList = require('./components/HeadersList')
const ResponseBody = require('./components/ResponseBody')

const colorsMap = {
  green: '#61ca7f',
  yellow: '#ebca7b',
  red: '#eb7f7b',
}

const getColorByStatus = (statusCode) => {
  return colorsMap[getStatusCodeColor(String(statusCode))]
}

const Container = styled.div`
  margin: auto;
  max-width: 700px;
`

const Header = styled.header`
  padding: 1rem;
  display: flex;
  align-items: center;
  background-color: #eaecf2;
  border-radius: 10px;
  color: #545865;

  > *:not(:last-child) {
    margin-right: 1rem;
  }
`

const Heading = styled.h3`
  color: #8a92a8;
`

const StatusBadge = styled.span`
  display: inline-block;
  background-color: ${({ statusCode }) => getColorByStatus(statusCode)};
  border: 4px solid #fff;
  border-radius: 50%;
  height: 16px;
  width: 16px;
`

const StatusCode = styled.span`
  color: ${({ children }) => getColorByStatus(children)};
  font-weight: bold;
`

const MethodName = styled.span`
  padding: 0.3rem 0.5rem;
  background-color: ${({ statusCode }) => getColorByStatus(statusCode)};
  border-radius: 5px;
  color: #fff;
  font-weight: bold;
  font-size: 0.8rem;
`
const Body = styled.main`
  padding: 1rem;
`

class Reporter extends React.Component {
  render() {
    const { request, url, statusCode, headers, body } = this.props

    return React.createElement(
      Container,
      null,
      React.createElement(
        Header,
        null,
        React.createElement(StatusBadge, { statusCode }),
        React.createElement(MethodName, { statusCode }, request.method),
        React.createElement(StatusCode, null, statusCode),
        React.createElement('span', null, url.href),
      ),
      React.createElement(
        Body,
        null,
        React.createElement(Heading, null, 'Response headers'),
        React.createElement(HeadersList, { headers }),
        body && React.createElement(Heading, null, 'Response body'),
        body && React.createElement(ResponseBody, { body }),
      ),
    )
  }
}

Reporter.propTypes = {
  request: PropTypes.object.isRequired,
  url: PropTypes.object.isRequired,
  statusCode: PropTypes.number.isRequired,
  headers: PropTypes.arrayOf(PropTypes.string).isRequired,
  body: PropTypes.string,
}

module.exports = Reporter
