const chalk = require('chalk')
const getStatusCodeColor = require('./getStatusCodeColor')

const formatStatusCode = (statusCode) => {
  const statusColor = getStatusCodeColor(String(statusCode))
  return chalk[statusColor](statusCode)
}

module.exports = formatStatusCode
