const R = require('ramda')

/**
 * Determines the status color based on the given
 * status code. Operates with "chalk" color keywords.
 */
const getStatusCodeColor = R.cond([
  [R.startsWith('20'), R.always('green')],
  [R.startsWith('30'), R.always('yellow')],
  [R.either(R.startsWith('40'), R.startsWith('50')), R.always('red')],
  [R.T, R.always('white')],
])

module.exports = getStatusCodeColor
