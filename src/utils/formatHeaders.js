const R = require('ramda')

const formatHeaders = R.compose(
  R.map(R.join(': ')),
  R.splitEvery(2),
)

module.exports = formatHeaders
