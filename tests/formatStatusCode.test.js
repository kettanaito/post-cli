const chalk = require('chalk')
const formatStatusCode = require('../src/utils/formatStatusCode')

test('Formats the given status code', () => {
  expect(formatStatusCode(200)).toEqual(chalk.green('200'))
  expect(formatStatusCode(301)).toEqual(chalk.yellow('301'))
  expect(formatStatusCode(404)).toEqual(chalk.red('404'))
  expect(formatStatusCode(500)).toEqual(chalk.red('500'))
})
