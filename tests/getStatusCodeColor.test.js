const getStatusCodeColor = require('../src/utils/getStatusCodeColor')

test('Returns proper color for the given status code', () => {
  expect(getStatusCodeColor('200')).toEqual('green')
  expect(getStatusCodeColor('301')).toEqual('yellow')
  expect(getStatusCodeColor('403')).toEqual('red')
  expect(getStatusCodeColor('500')).toEqual('red')
})
