const formatHeaders = require('../src/utils/formatHeaders')

test('Formats the given headers', () => {
  const rawHeaders = ['Connection', 'close', 'Content-Type', 'application/json']
  expect(formatHeaders(rawHeaders)).toEqual([
    'Connection: close',
    'Content-Type: application/json',
  ])
})
