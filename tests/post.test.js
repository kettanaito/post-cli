const post = require('../src')
const payload = JSON.stringify({ data: 'foo' })

test('Performs a POST request (200)', async () => {
  const response = await post('https://httpbin.org/post', payload)

  expect(response).toBeInstanceOf(Object)
  expect(response).toHaveProperty('statusCode', 200)
  expect(response).toHaveProperty('headers')
  expect(response).toHaveProperty('body')
  expect(response.headers.length).toBeGreaterThanOrEqual(1)
})

test('Performs a POST request (404)', async () => {
  const response = await post('https://httpbin.org/undefined', payload)

  expect(response).toBeInstanceOf(Object)
  expect(response).toHaveProperty('statusCode', 404)
  expect(response).toHaveProperty('headers')
  expect(response).toHaveProperty('body')
  expect(response.headers.length).toBeGreaterThanOrEqual(1)
})
