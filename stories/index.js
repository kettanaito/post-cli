import React from 'react'
import { storiesOf } from '@storybook/react'
import Reporter from '../src/reporters/htmlReporter/Reporter'
import '../src/reporters/htmlReporter/styles.css'

const url = {
  href: 'https://storybook.example',
}

const headers = [
  'Connection',
  'close',
  'Server',
  'gunicorn/19.9.0',
  'Date',
  'Tue, 23 Oct 2018 08:22:51 GMT',
  'Content-Type',
  'application/json',
  'Content-Length',
  '345',
  'Access-Control-Allow-Origin',
  '*',
  'Access-Control-Allow-Credentials',
  'true',
]

const body = `{
  "args": {},
  "data": "{ \"data\": \"Hello world\" }\n",
  "files": {},
  "form": {},
  "headers": {
    "Connection": "close",
    "Content-Length": "26",
    "Content-Type": "application/json",
    "Host": "https://storybook.example",
    "User-Agent": "post/1.0 (+https://apiary.io)"
  },
  "json": {
    "data": "Hello world"
  },
  "origin": "0.0.0.0",
  "url": "https://storybook.example"
}`

storiesOf('Reporter', module)
  .add('Success w/o body', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={200}
      headers={headers}
    />
  ))
  .add('Success with body', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={200}
      headers={headers}
      body={body}
    />
  ))
  .add('Redirect w/o body', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={301}
      headers={headers}
    />
  ))
  .add('Redirect with body', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={301}
      headers={headers}
      body={JSON.stringify({ message: 'Redirected for whatever reason' })}
    />
  ))
  .add('Failure w/o body', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={404}
      headers={headers}
    />
  ))
  .add('Failure with body', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={500}
      headers={headers}
      body={JSON.stringify({ error: 'Emulated failure, do not worry' })}
    />
  ))
  .add('Multiple headers', () => (
    <Reporter
      url={url}
      request={{ method: 'POST' }}
      statusCode={200}
      headers={[
        'Connection',
        'close',
        'Set-Cookie',
        '__Secure-ID=123; Secure; Domain=example.com',
        'Set-Cookie',
        '__Host-ID=123; Secure; Path=/',
        'Content-type',
        'application/json',
      ]}
      body={body}
    />
  ))
